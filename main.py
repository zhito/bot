import os
from dotenv import load_dotenv
import telebot
from telebot import types

load_dotenv()

API_TOKEN = os.getenv('API_TOKEN')
bot = telebot.TeleBot(API_TOKEN)


@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.reply_to(message, """\
Hi there, I am zhitogold_bot.
use /shop!\
""")


@bot.message_handler(commands=['shop'])
def send_welcome(message):
    markup = types.ReplyKeyboardMarkup()
    itembtna = types.KeyboardButton('/buy')
    itembtnb = types.KeyboardButton('/sell')
    markup.row(itembtna, itembtnb)
    bot.send_message(message.chat.id, "Choose one:", reply_markup=markup)


@bot.message_handler(commands=['buy'])
def send_welcome(message):
    text = "Напишите сумму в рублях (85руб.=100голды)"
    sent_msg = bot.send_message(message.chat.id, text, parse_mode="Markdown")
    bot.register_next_step_handler(sent_msg, money_handler)


@bot.message_handler(commands=['sell'])
def send_welcome(message):
    text = "Напишите сколько голды вы хотите продать"
    sent_msg = bot.send_message(message.chat.id, text, parse_mode="Markdown")
    bot.register_next_step_handler(sent_msg, gold_handler)


def money_handler(message):
    money = message.text
    text = f"Send {money} to Tinkoff {os.getenv('tin')}"
    bot.send_message(message.chat.id, text)


def gold_handler(message):
    gold = message.text
    text = f"Send {gold} to Standoff2 6536542443342"
    bot.send_message(message.chat.id, text)


bot.infinity_polling()
